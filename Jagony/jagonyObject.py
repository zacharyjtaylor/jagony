"""Defines the base object, which can create members upon reference."""

from utilities import *

class Jagony_Object(object):
	def __init__(self, name, fn = lambda: None):
		self.__dict__["_name"] = name
		self.__dict__["_fn"] = fn
	def _eval(self):
		return self.__dict__["_fn"]()
	def __call__(self, p):
		return Jagony_Object("$CALL", lambda: self.__dict__["_fn"](p))
	def Chain(self, obj):
		return Jagony_Object("$CHAIN", lambda: self._eval() + "\n" + obj._eval())
	def __getattr__(self, attr):
		if attr[0] == "_":
			return self.__dict__[attr]
		if attr not in self.__dict__.keys():
			self.__dict__[attr] = Jagony_Object(self._name + "." + attr)
		return self.__dict__[attr]
	def __setattr__(self, attr, val):
		self.__dict__[attr] = val
	def __repr__(self):
		return "Jagony_Object(" + self._name + " = " + str(self._fn) + ")"