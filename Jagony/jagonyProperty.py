""" Defines the class for `System.Types.Property` """

from jagonyObject import *

class Jagony_Property(Jagony_Object):
	def __init__(self, name):
		self.__dict__["_name"] = name
		self.__dict__["_fn"] = lambda: "_NS['" + name + "']"
