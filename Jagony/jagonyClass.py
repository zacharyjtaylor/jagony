"""Defines the class for `System.Types.Class`"""

from jagonyObject import *
from jagonyMethod import *
from jagonyProperty import *
from utilities import *

class Jagony_Class(Jagony_Object):
	def __init__(self, name):
		self.__dict__["_name"] = name._name
		self.__dict__["_fn"] = lambda: self._name + ".MainMethod._eval()"
	def DeclarePublicStaticProperty(self, name):
		Check_Validity(name, self)
		self.__dict__[GetLast(name)] = Jagony_Property(name._name)
		return Jagony_Object("$DECLARE_PUBLIC_STATIC_PROPERTY", lambda: "_NS['" + name._name + "'] = None")
	def DeclarePublicStaticMethod(self, name):
		Check_Validity(name, self)
		self.__dict__[GetLast(name)] = Jagony_Method(name._name)
		return JagonY_Object("$DECLARE_PUBLIC_STATIC_METHOD", lambda: "_NS['" + name._name + "'] = lambda: None")