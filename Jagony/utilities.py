"""General utilities"""
def Get_Last(obj):
	return obj._name.split(".")[-1]

def Check_Validity(member, parent):
	if member._name.split('.') != parent._name.split('.')[:-1]:
		raise SyntaxError(member._name + " should be a member of " + parent._name)
		exit()

def Indent_String(string):
	return "\n".join(list(map(lambda u:" "+u, string.split("\n"))))

def Pythonify_Name(name):
	res = ""
	for i in name:
		if i == ".":
			res += "_"
		else:
			res += i
	return res + "_"

def DEBUG(pr, val):
	print(pr)
	return val