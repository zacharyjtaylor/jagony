"""Defines everything to do with the "System" object."""

from jagonyObject import *
from jagonyClass import *
from jagonyProperty import *
from jagonyMethod import *

System = Jagony_Object("System")
System.Types.Class.Declare = Jagony_Class
System.Types.Property.Declare = Jagony_Property
System.Types.Method.Declare = Jagony_Method