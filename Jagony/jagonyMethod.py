"""Defines the class for `System.Types.Method`"""

from jagonyObject import *
from jagonyProperty import *
from utilities import *

class Jagony_Method(Jagony_Object):
	def __init__(self, name, fn = lambda: None):
		self.__dict__["_name"] = name
		self.__dict__["_fn"] = lambda u: Pythonify_Name(name) + "(" + u._eval() + ")"
	def DeclareArgument(self, name):
		Check_Validity(name, self)
		self._argument = name._name
		self.__dict__[GetLast(name)] = Jagony_Property(name._name)
		return Jagony_Object("$DECLARE_ARGUMENT", lambda: "_NS['" + name._name + "'] = None")
	def DeclareReturnValue(self, name):
		Check_Validity(name, self)
		self._return = name._name
		self.__dict__[GetLast(name)] = Jagony_Property(name._name)
		return Jagony_Object("$DECLARE_RETURN_VALUE", lambda: "_NS['" + name._name + "'] = None")
	def Define(self, par):
		string = "def " + Pythonify_Name(self._name) + \
			"(_ARGUMENT):\n " + self._argument._eval() + " = _ARGUMENT\n" + \
			Indent_Code(par._eval()) + "\n return " + self._return._eval()
		return Jagony_Object("$DEFINE", lambda: string)