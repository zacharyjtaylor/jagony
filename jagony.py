class Undefined: #Class for undefined
	def __init__(self):
		pass
	def __eq__(self,o):
		return False
	def __getattr__(self,name):
		print("TRYING TO GET " + name + " PROPERTY FROM UNDEFINED")
		exit()

undefined = Undefined() #A variable to determine if an object is not defined yet, or "does not exist".

class JagonyObject:
	def __init__(self, name, val = None):
		self.__dict__["_name"] = name
		self.__dict__["_val"] = val
	def _eval(self):
		self._val()
	def __call__(self, p):
		return JagonyObject("$CALL",lambda: self._val(p))
	def __getattr__(self, name):
		if name[0] == "_":
			return self.__dict__[name]
		else:
			if not(name in self.__dict__.keys()):
				self.__dict__[name] = JagonyObject(self._name + "." + name, lambda u: None) #Don't know why this works
				if self._val == None:
					self._val = undefined
			return self.__dict__[name]
	def __setattr__(self, name, val):
		self.__dict__[name] = val
		if self._val == None:
			self._val = undefined
	def __str__(self):
		return self.__repr__()
	def __repr__(self):
		return "JagonyObject(" + self._name + ", " + str(self._val) + ")"
	def Chain(self, o):
		def F():
			self._eval()
			o._eval()
		return JagonyObject("$CHAIN", F)

System = JagonyObject("System")

def System_Types_Class_Declare(name):
	def PublicVisibility_DeclareStaticProperty(prop):
		if ".".join(prop._name.split(".")[:-1]) != name._name:
			raise SyntaxError("Cannot declare `" + prop._name + "` inside of `" + name._name + "`")
			exit()
		prop = JagonyObject(prop._name, undefined)

	name.PublicVisibility.DeclareStaticProperty = JagonyObject(
		name._name + ".PublicVisibility.DeclareStaticProperty",
		PublicVisibility_DeclareStaticProperty
	)

System.Types.Class.Declare = JagonyObject("System.Types.Class.Declare",System_Types_Class_Declare)

def ExecuteJagony(arg):
	exec(arg+"._eval()")